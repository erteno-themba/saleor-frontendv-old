module.exports = {
  client: {
    excludes: ["**/__tests__/**/*", "**/@sdk/**/*"],
    service: {
      name: "cooksrepublic",
      url: "https://cooksrepublic.herokuapp.com/graphql/",
    },
  },
};
